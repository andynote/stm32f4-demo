/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2021-10-19     RT-Thread    first version
 */

#include <rtthread.h>
#include <board.h>
#include <rtdevice.h>

#define DBG_TAG "main"
#define DBG_LVL DBG_LOG
#include <rtdbg.h>

// main 线程
int main(void)
{
    int count = 1;

    LOG_D("Hello RT-Thread! \r\n");

    // main
    rt_base_t led_pin = GET_PIN(F, 9);
    // LED mode
    rt_pin_mode(led_pin, PIN_MODE_OUTPUT);

    while (count++)
    {
        rt_pin_write(led_pin, PIN_HIGH);
        rt_thread_mdelay(500);
        rt_pin_write(led_pin, PIN_LOW);
        rt_thread_mdelay(500);
    }

    return RT_EOK;
}


