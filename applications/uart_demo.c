/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2021-10-23     Andy       the first version
 */
#include <rtthread.h>
#include <board.h>
#include <rtdevice.h>

#define DBG_TAG "uart_demo"
#define DBG_LVL DBG_LOG
#include <rtdbg.h>

#define THREAD_PRIORITY         25
#define THREAD_STACK_SIZE       512
#define THREAD_TIMESLICE        5

// 独立PIN线程
static void thread1_entry(void *parameter)
{
    rt_device_t uart_dev = rt_device_find("uart1");
    rt_err_t err = rt_device_open(uart_dev, RT_DEVICE_FLAG_RDWR );
    if(err!=RT_EOK){
        LOG_D("Uart Open error %d. \r\n", err);
        return;
    }
    int count = 1;
    char buff[128];
    while (1)
    {
        rt_sprintf(buff, "Uart Date %d.\r\n", count);
        rt_device_write(uart_dev, 0, buff, rt_strlen(buff));
        count++;
        rt_thread_mdelay(1000);
    }
}

int uart_demo(void)
{

    LOG_D("Uart Demo Thread. \r\n");
    // thread 1
    rt_thread_t tid1 = rt_thread_create("uart_demo",
                    thread1_entry, RT_NULL,
                    THREAD_STACK_SIZE,
                    THREAD_PRIORITY, THREAD_TIMESLICE);
    rt_thread_startup(tid1);

    return 0;
}

/* 导出到 msh 命令列表中 */
MSH_CMD_EXPORT(uart_demo, uart export demo);


