/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2021-10-23     Andy       the first version
 */
#include <rtthread.h>
#include <board.h>
#include <rtdevice.h>

#define DBG_TAG "pin_demo"
#define DBG_LVL DBG_LOG
#include <rtdbg.h>

#define THREAD_PRIORITY         25
#define THREAD_STACK_SIZE       512
#define THREAD_TIMESLICE        5

// 独立PIN线程
static void thread1_entry(void *parameter)
{
    rt_base_t led_pin = GET_PIN(F, 10);
    // LED mode
    rt_pin_mode(led_pin, PIN_MODE_OUTPUT);

    while (1)
    {
        rt_pin_write(led_pin, PIN_HIGH);
        rt_thread_mdelay(1500);
        rt_pin_write(led_pin, PIN_LOW);
        rt_thread_mdelay(1500);
    }
}

int pin_demo(void)
{

    LOG_D("Pin Demo Thread. \r\n");
    // thread 1
    rt_thread_t tid1 = rt_thread_create("pin_demo",
                    thread1_entry, RT_NULL,
                    THREAD_STACK_SIZE,
                    THREAD_PRIORITY, THREAD_TIMESLICE);
    rt_thread_startup(tid1);

    return 0;
}

/* 导出到 msh 命令列表中 */
MSH_CMD_EXPORT(pin_demo, pin export demo);


